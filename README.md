# Dungeon RPG #

Escape the enemy skeletons, find the door and go to the next level.

Small game written in Javascript. The goal of this project was to experiment with A* Pathfinding for AI. As you progress, the AI speed increases. See what's the maximum level you can reach!