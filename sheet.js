(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame =
    window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
    window.requestAnimationFrame = function(callback, element) {
        var currTime = new Date().getTime();
        var timeToCall = Math.max(0, 16 - (currTime - lastTime));
        var id = window.setTimeout(function() { callback(currTime + timeToCall); },
            timeToCall);
        lastTime = currTime + timeToCall;
        return id;
    };

if (!window.cancelAnimationFrame)
    window.cancelAnimationFrame = function(id) {
        clearTimeout(id);
    };
}());

var Enemy = function(x, y) {
    this.pos = {
        x: x,
        y: y
    };

    this.sprite = Math.random() > 0.5 ? 53 : 54;
    this._last = Date.now();
    this.path = [];
    this.pathIndex=-1;
    this.AIInterval = 1500;
};

Enemy.prototype.update = function(t) {
    var AIInterval = this.AIInterval;   
    var lastPos = this.pos;

    if (t - this._last > AIInterval) {
        if (this.path.length && this.pathIndex >= -1 && this.pathIndex < this.path.length - 1) {
            try {
            this.pos = {
                x: this.path[++this.pathIndex].y,
                y: this.path[this.pathIndex].x
            };
            } catch (e) {
                console.log('pathindex:' + this.pathIndex);
                console.log(this.path[this.pathIndex]);
                console.log(e);
            }
            this._last = t;
        }
    }

    for (var eId = enemies.length; --eId;) {
        if (this === enemies[eId]) {
            continue;
        }
        var otherEnemy = enemies[eId];
        if (this.collidesWith(otherEnemy)) {
            this.pos = lastPos;
            this.pathIndex--;
            break;
        }
    }

    if (this.collidesWith({pos:{x:hero.x,y:hero.y}})) {
        owSound.play();
        hero.health -= 10;
        this.pos = lastPos;
        this.pathIndex--;
    }
};

Enemy.prototype.collidesWith = function(enemy) {
    if (this.pos.x == enemy.pos.x && this.pos.y == enemy.pos.y)
        return true;
    return false;
};

var stats,
    DEBUG = !0,
    map = {
        //W: 8,
        //H: 8,
        //data: [8, 8, 8, 11, 8, 8, 8, 8, 8, 0, 0, 0, 13, 0, 0, 8, 8, 0, 0, 0, 0, 0, 3, 8, 8, 0, 0, 0, 0, 0, 0, 8, 8, 0, 0, 0, 0, 0, 0, 8, 8, 0, 0, 0, 0, 0, 3, 8, 8, 5, 0, 0, 0, 0, 0, 8, 8, 8, 8, 8, 8, 8, 8, 8]
    },
    mapGraph = [],
    pathToDoor = [],
    hero = new Hero(2, 2),
    nEnemies = 3,
    enemies = [],
    sound = new Howl({
        urls: ['res/step.ogg', 'res/step.wav'],
        volume: 0.2
    }),
    doorSound = new Howl({
        urls: ['res/door.ogg'],
    pos: 0.4
    }),
    hitSound = new Howl({
        urls: ['res/hit.ogg'],
    volume: 0.2
    }),
    owSound = new Howl({
        urls: ['res/ow.wav'],
        volume: 0.2
    }),
    themeSound = new Howl({
        urls: ['res/theme.mp3'],
        volume: 0.1,
        autoplay: false,
        loop: true
    }),
    /*
     * 0: floor
     * 1: pillar
     * 5: stairs_down
     * 5: stairs_up
     * 8: wall
     * 11: door
     * 13: spikes
     * 15: water
     * 16: chest
     */
    TILES = {
        floor: 0,
        pillar: 1,
        stairs_down: 5,
        stairs_up: 5,
        wall: 8,
        door: 11,
        spikes: 13,
        water: 15,
        chest: 16
    },
    tileWeights = {
        floor: 67,
        pillar: 9,
        spikes: 2
        //stairs_down: 1
    },
    tileWeightSum = 0,
    // current game round
    currentRound = 0;

function weightedChoice()
{
    var rnd = Math.random() * tileWeightSum;
    for (var prop in tileWeights) {
       rnd -= tileWeights[prop];
       if (rnd < 0) 
           return TILES[prop];
    }
}

function randomMap(width, height) {
    var x, y, doorx, doory;

    var generatedMap = {};
    generatedMap.data = new Array(width * height);
    generatedMap.W = width;
    generatedMap.H = height;

    // set door
    //doorx = Math.random() * (width - 3) | 0 + 1;
    //doory = 0;
    //generatedMap.data[doorx + doory * width] = TILES.door;
    for(x = 0; x < width; x++) {
        for(y = 0; y < height; y++) {
            if(y === 0 || x === 0 || y == height - 1 || x == width - 1) {
                generatedMap.data[x + y * width] = TILES.wall;
                continue;
            }
            generatedMap.data[x + y * width] = weightedChoice();
        }
    }

    generatedMap.time = Date.now();
    generatedMap.doorPlaced = false;

    return generatedMap;
}

function generateGraphFromMap(map) {
    var x, y, width = map.W,
        grid = [];

    // Walkable tiles are: floor, stairs_*, door
    for(y = 0; y < map.H; y++) {
        grid[y] = [];
        for(x = 0; x < width; x++) {
            switch(map.data[x + y * width]) {
                case TILES.floor:
                case TILES.stairs_down:
                case TILES.stairs_up:
                case TILES.door:
                    grid[y][x] = 1;
                    break;
                default:
                    grid[y][x] = 0;
                    break;
            }
        }
    }

    return new Graph(grid);
}

function recalculatePath(start, goal) {
    if(!goal) return;
    return astar.search(mapGraph.nodes, mapGraph.nodes[start.y][start.x], mapGraph.nodes[goal.y][goal.x]);
}

function isWalkable(tileId) {
    switch(tileId) {
        case TILES.floor:
        case TILES.door:
        case TILES.stairs_down:
            return true;
        default:
            return false;
    }
}

function replaceDoor() {
    var doorIndex = map.data.indexOf(TILES.door),
        newDoorIndex = Math.random()*(map.W-2)+1|0;

    var testPath = recalculatePath({x: newDoorIndex, y: 0}, enemies[0].pos);
    var tries = 10;
    while (!testPath.length && tries--) {
        newDoorIndex = Math.random()*(map.W-2)+1|0;
        testPath = recalculatePath({x: newDoorIndex, y: 0}, enemies[0].pos);
    }

    if (doorIndex != -1) {
        map.data[doorIndex] = TILES.wall;
    }
    map.data[newDoorIndex] = TILES.door;

    mapGraph = generateGraphFromMap(map);
    pathToDoor = recalculatePath(hero, {x: newDoorIndex, y: 0});
}

function playTileSound(tileId) {
    switch(tileId) {
        case TILES.floor:
            Math.random() < 0.3 && sound.play();
            break;
        case TILES.door:
            doorSound.play();
            break;
        case TILES.stairs_down:
            // stairs
            break;
        default:
            hitSound.play();
            break;
    }
}

function keyHandler(key) {
    var oldx = hero.x,
        oldy = hero.y,
        tileIndex, tileId;

    switch(key.keyCode) {
        case 65:
            // A
            hero.x -= 1;
            break;
        case 68:
            // D
            hero.x += 1;
            break;
        case 83:
            // W
            hero.y += 1;
            break;
        case 87:
            // W
            hero.y -= 1;
            break;
        case 77:
            // M
            themeSound.pause();
            break;
        default:
            return;
    }
    tileIndex = hero.x + hero.y * map.W|0;
    tileId = map.data[tileIndex];

    // Now for the sounds
    playTileSound(tileId);

    if(!isWalkable(tileId)) {
        hero.x = oldx;
        hero.y = oldy;
    }

    if(tileId == TILES.door) {
        var doorIndex;
        currentRound++;
        newRoom(32, 32);
    }

    for(var eId = 0; eId < enemies.length; eId++) {
        var enemy = enemies[eId];
        enemy.path = recalculatePath(enemy.pos, hero);
        enemy.pathIndex = -1;
    }

}

var setup = function() {
    c.width = window.innerWidth;
    c.height = window.innerHeight;

    Math.seedrandom();

    stats = new Stats();
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.right = '0px';
    stats.domElement.style.top = '0px';

    (function() {
        var lastTime = 0;
        var vendors = ['ms', 'moz', 'webkit', 'o'];
        for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
            window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
            window.cancelAnimationFrame =
        window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
        }

        if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
    }());

    rpg.graphics.init(a, 'res/tiles.png');

    b.appendChild(stats.domElement);
    b.addEventListener('keydown', keyHandler, false);

    start();
};

function newRoom(W, H) {
    var width, height;

    width = W || 16;
    height = H || 16;

    map = randomMap(width, height);
    mapGraph = generateGraphFromMap(map);

    hero.x = Math.random() * map.W | 0;
    hero.y = Math.random() * map.H | 0;
    while(!isWalkable(map.data[hero.x + map.W * hero.y])) {
        hero.x = Math.random() * map.W | 0;
        hero.y = Math.random() * map.H | 0;
    }
    pathToDoor = [];

    enemies = [];
    for(var eId = 0; eId < nEnemies; eId++) {
        var enemy = new Enemy(Math.random() * map.W | 0, Math.random() * map.H | 0);

        while(!isWalkable(map.data[enemy.pos.x + map.W * enemy.pos.y])) {
            enemy.pos.x = Math.random() * map.W | 0;
            enemy.pos.y = Math.random() * map.H | 0;
        }
        enemy.path = recalculatePath(enemy.pos, hero);
        enemy.AIInterval = 1500 - 160*currentRound;
        enemies.push(enemy);
    }


}

var start = function() {

    for (var prop in tileWeights) {
        tileWeightSum += tileWeights[prop];
    }
    newRoom(32, 32);
    gameLoop();
};


var gameLoop = function() {
    stats.begin();
    requestAnimationFrame(gameLoop);
    run();
    stats.end();
};  

var update = function(t) {
    if (!map.doorPlaced && t - map.time > 20000) {
        replaceDoor();
        map.doorPlaced = true;
    }

    for(var eId = 0; eId < enemies.length; eId++) {
        var enemy = enemies[eId];
        enemy.update(t);
    }

    if (hero.health <= 0) {
        hero.health = 100;
        currentRound = 0;
        newRoom(32, 32);
    }
};

var draw = function() {
    var x, y,
        enemy;

    a.clearRect(0, 0, c.width, c.height);
    for(x = 0; x < map.W; x++) {
        for(y = 0; y < map.H; y++) {
            rpg.graphics.drawTile(map.data[x + map.W * y], x, y);
        }
    }

    rpg.graphics.drawTile(hero.sprite, hero.x, hero.y);

    if (Date.now() - map.time < 2500) {
        rpg.graphics.drawAttention(hero.x, hero.y);
    }

    if (hero.health <= 25)
        a.fillStyle = 'red';
    else if (hero.health <= 50)
        a.fillStyle = 'orange';
    else
        a.fillStyle = 'green';
    a.fillRect(hero.x*32, hero.y*32-4, 32*hero.health/100|0,3);

    for(var eId = 0; eId < enemies.length; eId++) {
        enemy = enemies[eId];
        rpg.graphics.drawTile(enemy.sprite, enemy.pos.x, enemy.pos.y);
    }

    // draw overlay
    if(DEBUG) {
        a.fillStyle = rpg.graphics.DEBUG_OVERLAY_FILLSTYLE;
        for(eId = 0; eId < enemies.length; eId++) {
            enemy = enemies[eId];
            if(!enemy.path.length) continue;
            rpg.graphics.drawPath(enemy.path);
        }
        if (pathToDoor.length) {
            rpg.graphics.drawPath(pathToDoor, 'rgba(10,255,0,0.3)');
        }
    }

    rpg.graphics.drawStats(currentRound);

};

var run = (function() {
    var loops = 0,
    skipTicks = 1000 / 30,
    maxFrameSkip = 10,
    nextGameTick = (new Date).getTime();

    return function() {
        loops = 0;

        var t = Date.now();

        while(t > nextGameTick && loops < maxFrameSkip) {
            update(t);
            nextGameTick += skipTicks;
            loops++;
        }

        draw();
    };
})();

setup();
