#!/bin/sh

files=$(git ls-files)
DROPBOX_PUBLIC_DIR=$HOME/Dropbox/Public/rp

rsync -Ru $files $DROPBOX_PUBLIC_DIR/

# disable debugging
sed -i '' -e's/DEBUG = !0/DEBUG = !1/' $DROPBOX_PUBLIC_DIR/sheet.js
