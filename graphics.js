rpg.graphics = (function(){
    var my = {};

    my._ready = false;
    var tileset = {};

    my.DEBUG_OVERLAY_FILLSTYLE = 'rgba(255,0,0,0.2)';

    my.init = function(ctx, sheetUrl) {
        my.ctx = ctx;
        my.ctx.font = 'bold 30px Arial';

        my.ctx.lineWidth = 4;

        var w = ctx.canvas.width,
            h = ctx.canvas.height;

        tileset.img = new Image();
        tileset.img.onload = function() {
            my._ready = true;
        };
        tileset.img.src = sheetUrl;

        tileset.TILE_SIZE = 32;
        tileset.TILES_X = 8;
        tileset.TILES_Y = 8;
    };

    my.drawTile = function(id, x, y) {
        my.ctx.drawImage(tileset.img,
            (id % tileset.TILES_X) * tileset.TILE_SIZE,
            (id / tileset.TILES_X | 0) * tileset.TILE_SIZE,
            tileset.TILE_SIZE,
            tileset.TILE_SIZE,
            x * tileset.TILE_SIZE | 0,
            y * tileset.TILE_SIZE | 0,
            tileset.TILE_SIZE | 0,
            tileset.TILE_SIZE | 0);
    };

    my.drawTileFree = function(id, x, y) {
        my.ctx.drawImage(tileset.img,
            (id % tileset.TILES_X) * tileset.TILE_SIZE,
            (id / tileset.TILES_X | 0) * tileset.TILE_SIZE,
            tileset.TILE_SIZE,
            tileset.TILE_SIZE,
            x | 0,
            y | 0,
            tileset.TILE_SIZE | 0,
            tileset.TILE_SIZE | 0);
    };

    my.drawPath = function(path, style) {
        my.ctx.fillStyle = style || my.DEBUG_OVERLAY_FILLSTYLE;
        for (var i=0, len=path.length; i<len; i++) {
            my.ctx.fillRect(path[i].y * tileset.TILE_SIZE, path[i].x * tileset.TILE_SIZE, tileset.TILE_SIZE, tileset.TILE_SIZE);
        }
    };

    my.drawAttention = function(x, y, style) {
        my.ctx.strokeStyle = style || '#E53E3E';
        my.ctx.beginPath();
        my.ctx.arc((x + 0.5) * tileset.TILE_SIZE, (y + 0.5) * tileset.TILE_SIZE, 20, 0, 2*Math.PI, false);
        my.ctx.stroke();
    };

    my.drawStats = function(currentRound) {
        my.ctx.fillStyle = 'black';
        my.ctx.fillText("Round: " + currentRound, 10, 30);
    };

    return my;
}());
